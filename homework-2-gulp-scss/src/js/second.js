"use strict";

function burgerMenu() {
	const buttons = document.querySelector(".btn__icon, .nav__btn");
	const menuIcons = document.querySelector(".title__menu-burger");
	buttons.addEventListener("click", () => {
		menuIcons.classList.toggle("burger__active");
	});
	function selectLink() {
		const list = document.querySelectorAll(".title__menu-link");
		list.forEach((elem) => {
			elem.addEventListener("click", () => {
				if (!elem.classList.contains("active")) {
					list.forEach((item) => {
						item.classList.remove("active");
					});
					elem.classList.add("active");
				};

			});
		});
	};
	selectLink();
};
burgerMenu();
// const menuIcons = document.querySelector(".nav__btn"),
// 	icon = document.querySelector('btn__icon'),
// 	link = document.querySelectorAll('.title__menu-link');
// if (menuIcons) {
// 	const items = document.querySelector(".items");
// 	menuIcons.addEventListener("click", e => {
// 		menuIcons.classList.toggle("active"),
// 			items.classList.toggle("active"),
// 			icon.classList.toggle("change")
// 	}
// 	)
// }

// link.forEach(element => {
// 	element.addEventListener('click', l => {
// 		let item = element
// 		link.forEach(el => {
// 			el.classList.remove('active')
// 		})
// 		item.classList.add('active')
// 	})
// });
