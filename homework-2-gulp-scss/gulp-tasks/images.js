const { src, dest } = require("gulp");
const { bs } = require("./serv.js");
const imagemin = require('gulp-imagemin');


const images = () => {
   src("./src/images/**/*.{jpg,jpeg,png,gif,tiff,svg}")
   .pipe(imagemin({progressive: true}))
   .pipe(dest("./dist/images"))
};

exports.images = images;
